import requests, json, datetime, threading,os,sys, time

global Classroom, section1_starttime, count, week
global section1_starttime, section1_endtime, section1_curriculum, section2_starttime, section2_endtime, section2_curriculum, section3_starttime, sectoin3_endtime, section3_curriculum, section4_starttime, sectoin4_endtime, section4_curriculum, section5_starttime, sectoin5_endtime, section5_curriculum, section6_starttime, sectoin6_endtime, section6_curriculum, section7_starttime, sectoin7_endtime, section7_curriculum, section8_starttime, section8_endtime, section8_curriculum, section9_starttime, section9_endtime, section9_curriculum, section10_starttime, sectoin10_endtime, section10_curriculum, section11_starttime, section11_endtime, section11_curriculum, section12_starttime, sectoin12_endtime, section12_curriculum, section13_starttime, section13_endtime, section13_curriculum, section14_starttime, sectoin14_endtime, section14_curriculum
count = 0
Classroom = 'R402'
def get_week():
    week = datetime.datetime.now().strftime('%w')
    if week != 0:
        return(int(week))
    else:
        return(7)
week = get_week()
class_start_time = '00:00'
token_url ='http://rcp.nctu.me:8000/api/token/'
get_class_url = 'http://rcp.nctu.me:8000/api/curriculum/subjects/'
get_beacon_url = 'http://rcp.nctu.me:8000/api/roll_call/beacons/'
token_json = {'username': 'admin', 'password':'123@Admin'}
token_header = {'Content-Type': 'application/json'}

post_token  =  requests.post(url=token_url,json= token_json, headers = token_header)
token = post_token.json()
refresh_token = token ['refresh']
access_token = token['access']

class_header = {'Authorization':'Bearer '+access_token}

def get_class_request():
    global class_json
    get_class = requests.get(url=get_class_url, headers=class_header)
    class_json = get_class.json()
get_class_request()

def get_beacon_request():
    global class_beacon
    get_beacon = requests.get(url=get_beacon_url, headers=class_header)
    class_beacon = get_beacon.json()
get_beacon_request()

def init():
    global section1_starttime, section1_endtime, section1_curriculum, section2_starttime, section2_endtime, section2_curriculum, section3_starttime, sectoin3_endtime, section3_curriculum, section4_starttime, sectoin4_endtime, section4_curriculum, section5_starttime, sectoin5_endtime, section5_curriculum, section6_starttime, sectoin6_endtime, section6_curriculum, section7_starttime, sectoin7_endtime, section7_curriculum, section8_starttime, section8_endtime, section8_curriculum, section9_starttime, section9_endtime, section9_curriculum, section10_starttime, sectoin10_endtime, section10_curriculum, section11_starttime, section11_endtime, section11_curriculum, section12_starttime, sectoin12_endtime, section12_curriculum, section13_starttime, section13_endtime, section13_curriculum, section14_starttime, sectoin14_endtime, section14_curriculum
    section1_starttime='00:00'
    section1_endtime='00:00'
    section1_curriculum='00:00'
    section2_starttime=''
    section2_endtime=''
    section2_curriculum=''
    section3_starttime=''
    sectoin3_endtime=''
    section3_curriculum=''
    section4_starttime=''
    sectoin4_endtime=''
    section4_curriculum=''
    section5_starttime=''
    sectoin5_endtime=''
    section5_curriculum=''
    section6_starttime=''
    sectoin6_endtime=''
    section6_curriculum=''
    section7_starttime=''
    sectoin7_endtime=''
    section7_curriculum=''
    section8_starttime=''
    section8_endtime=''
    section8_curriculum=''
    section9_starttime=''
    section9_endtime=''
    section9_curriculum=''
    section10_starttime=''
    sectoin10_endtime=''
    section10_curriculum=''
    section11_starttime=''
    section11_endtime=''
    section11_curriculum=''
    section12_starttime=''
    sectoin12_endtime=''
    section12_curriculum=''
    section13_starttime=''
    section13_endtime=''
    section13_curriculum=''
    section14_starttime=''
    sectoin14_endtime=''
    section14_curriculum=''

init()


# get_section_times below==========================================

def get_section_times():
    global section1_starttime, section1_endtime, section1_curriculum, section2_starttime, section2_endtime, section2_curriculum, section3_starttime, sectoin3_endtime, section3_curriculum, section4_starttime, sectoin4_endtime, section4_curriculum, section5_starttime, sectoin5_endtime, section5_curriculum, section6_starttime, sectoin6_endtime, section6_curriculum, section7_starttime, sectoin7_endtime, section7_curriculum, section8_starttime, section8_endtime, section8_curriculum, section9_starttime, section9_endtime, section9_curriculum, section10_starttime, sectoin10_endtime, section10_curriculum, section11_starttime, section11_endtime, section11_curriculum, section12_starttime, sectoin12_endtime, section12_curriculum, section13_starttime, section13_endtime, section13_curriculum, section14_starttime, sectoin14_endtime, section14_curriculum
    for i in class_json:
        if i['class_room'] == Classroom:
            for o in i['section_times']:
                if o['section'] == 1 and o['week'] == week:
                    section1_starttime= o['start_time']
                    section1_endtime = o['end_time']
                    section1_curriculum = i['id']
                    print(section1_endtime)
                if o['section'] == 2 and o['week'] == week:
                    section2_starttime = o['start_time']
                    section2_endtime = o['end_time']
                    section2_curriculum = i['id']
                if o['section'] == 3 and o['week'] == week:
                    section3_starttime = o['start_time']
                    sectoin3_endtime = o['end_time']
                    section3_curriculum = i['id']
                if o['section'] == 4 and o['week'] == week:
                    section4_starttime = o['start_time']
                    sectoin4_endtime = o['end_time']
                    section4_curriculum = i['id']
                if o['section'] == 5 and o['week'] == week:
                    section5_starttime = o['start_time']
                    sectoin5_endtime = o['end_time']
                    section5_curriculum = i['id']
                if o['section'] == 6 and o['week'] == week:
                    section6_starttime = o['start_time']
                    sectoin6_endtime = o['end_time']
                    section6_curriculum = i['id']
                if o['section'] == 7 and o['week'] == week:
                    section7_starttime = o['start_time']
                    sectoin7_endtime = o['end_time']
                    section7_curriculum = i['id']
                if o['section'] == 8 and o['week'] == week:
                    section8_starttime = o['start_time']
                    section8_endtime = o['end_time']
                    section8_curriculum = i['id']
                if o['section'] == 9 and o['week'] == week:
                    section9_starttime = o['start_time']
                    section9_endtime = o['end_time']
                    section9_curriculum = i['id']
                if o['section'] == 10 and o['week'] == week:
                    section10_starttime = o['start_time']
                    sectoin10_endtime = o['end_time']
                    section10_curriculum = i['id']
                if o['section'] == 11 and o['week'] == week:
                    section11_starttime = o['start_time']
                    section11_endtime = o['end_time']
                    section11_curriculum = i['id']
                if o['section'] == 12 and o['week'] == week:
                    section12_starttime = o['start_time']
                    sectoin12_endtime = o['end_time']
                    section12_curriculum = i['id']
                if o['section'] == 13 and o['week'] == week:
                    section13_starttime = o['start_time']
                    section13_endtime = o['end_time']
                    section13_curriculum = i['id']
                if o['section'] == 14 and o['week'] == week:
                    section14_starttime = o['start_time']
                    sectoin14_endtime = o['end_time']
                    section14_curriculum = i['id']

get_section_times()
# get_beacon below==========================================

def get_beacon(class_id):
    global beacon_uuid, beacon_major, beacon_minor
    for i in class_beacon:
        if i['curriculum'] == class_id:
            beacon_uuid = str(i['beacon_uuid'])
            beacon_major = format(i['beacon_major'], 'x')
            beacon_minor = format(i['beacon_minor'], 'x')

# date_formater below==========================================

def date_formater(section_time):
    format = '%H:%M'
    format_time = datetime.datetime.strptime(section_time,format)
    return(format_time)

# send_beacon below==========================================

def send_beacon():
    global beacon_uuid, beacon_major, beacon_minor
    print('send' + str(datetime.datetime.now()))
    os.system("sudo hciconfig hci0 up")
    os.system("sudo hciconfig hci0 leadv 3")
    os.system("sudo hciconfig hci0 noscan")
    # beacon = beacon_uuid.replace('-','')+beacon_major+beacon_minor
    # format_beacon = ' '.join(beacon[i:i + 2] for i in range(0, len(beacon), 2))
    beacon = '2f 7c 16 5b 51 46 48 a2 ae 6b d7 ac 7c 88 52 46'
    major =' dd 6d'
    minor = 'cc 1a'
    cmd = "sudo hcitool -i hci0 cmd 0x08 0x0008 1E 02 01 1A 1A FF 4C 00 02 15 "
    os.system(cmd + beacon+major+minor)


# send_beacon_off below==========================================

def send_beacon_off():
    os.system("sudo hciconfig hci0 down")

# timber below=========================================='

def counter_timer():
    global timer
    global count
    timer = threading.Timer(1, counter_timer)
    timer.start()

    count += 1
    if (count == 20):  # rollcall time per once :180
        timer.cancel()
        sendoff_timer()


def sendoff_timer():
    global count
    print('sendoff' + str(datetime.datetime.now()))
    send_beacon_off()
    count = 0


def total_timer():
    send_beacon()
    timer2 = threading.Timer(120, total_timer)  # rollcall frequency :600s
    timer2.start()
    if datetime.datetime.today().strftime('%H:%M') in (section1_endtime , section2_endtime , sectoin3_endtime , sectoin4_endtime , sectoin5_endtime , sectoin6_endtime , sectoin7_endtime , section8_endtime , section9_endtime , sectoin10_endtime , section11_endtime , sectoin12_endtime , section13_endtime , sectoin14_endtime) and (datetime.datetime.today().strftime('%H') == section1_endtime[:2] and datetime.datetime.today().strftime('%M') > section1_endtime[:-2] or datetime.datetime.today().strftime('%H') == section2_endtime[:2] and datetime.datetime.today().strftime('%M') > section2_endtime[:-2] or datetime.datetime.today().strftime('%H') == sectoin3_endtime[:2] and datetime.datetime.today().strftime('%M') > sectoin3_endtime[:-2] or datetime.datetime.today().strftime('%H') == sectoin4_endtime[:2] and datetime.datetime.today().strftime('%M') > sectoin4_endtime[:-2] or datetime.datetime.today().strftime('%H') == sectoin5_endtime[:2] and datetime.datetime.today().strftime('%M') > sectoin5_endtime[:-2] or datetime.datetime.today().strftime('%H') == sectoin6_endtime[:2] and datetime.datetime.today().strftime('%M') > sectoin6_endtime[:-2] or datetime.datetime.today().strftime('%H') == sectoin7_endtime[:2] and datetime.datetime.today().strftime('%M') > sectoin7_endtime[:-2] or datetime.datetime.today().strftime('%H') == section8_endtime[:2] and datetime.datetime.today().strftime('%M') > section8_endtime[:-2] or datetime.datetime.today().strftime('%H') == section9_endtime[:2] and datetime.datetime.today().strftime('%M') > section9_endtime[:-2] or datetime.datetime.today().strftime('%H') == sectoin10_endtime[:2] and datetime.datetime.today().strftime('%M') > sectoin10_endtime[:-2] or datetime.datetime.today().strftime('%H') == section11_endtime[:2] and datetime.datetime.today().strftime('%M') > section11_endtime[:-2] or datetime.datetime.today().strftime('%H') == sectoin12_endtime[:2] and datetime.datetime.today().strftime('%M') > sectoin12_endtime[:-2] or datetime.datetime.today().strftime('%H') == section13_endtime[:2] and datetime.datetime.today().strftime('%M') > section13_endtime[:-2] or datetime.datetime.today().strftime('%H') == sectoin14_endtime[:2] and datetime.datetime.today().strftime('%M') > sectoin14_endtime[:-2]):
        send_beacon_off()
        timer2.cancel()
        starter()

    counter_timer()


# starter below==========================================

def starter():
    global section1_starttime, section1_endtime, section1_curriculum, section2_starttime, section2_endtime, section2_curriculum, section3_starttime, sectoin3_endtime, section3_curriculum, section4_starttime, sectoin4_endtime, section4_curriculum, section5_starttime, sectoin5_endtime, section5_curriculum, section6_starttime, sectoin6_endtime, section6_curriculum, section7_starttime, sectoin7_endtime, section7_curriculum, section8_starttime, section8_endtime, section8_curriculum, section9_starttime, section9_endtime, section9_curriculum, section10_starttime, sectoin10_endtime, section10_curriculum, section11_starttime, section11_endtime, section11_curriculum, section12_starttime, sectoin12_endtime, section12_curriculum, section13_starttime, section13_endtime, section13_curriculum, section14_starttime, sectoin14_endtime, section14_curriculum
    get_class_request()
    get_beacon_request()
    while datetime.datetime.today().strftime('%H:%M') not in (section1_starttime , section2_starttime  , section3_starttime , section4_starttime , section5_starttime , section6_starttime , section7_starttime , section8_starttime , section9_starttime , section10_starttime , section11_starttime , section12_starttime , section13_starttime , section14_starttime) and datetime.datetime.today().strftime('%H') > '20':
        time.sleep(1)
    if  datetime.datetime.today().strftime('%H:%M') == section1_starttime or datetime.datetime.today().strftime('%H') < section1_endtime[:2] and datetime.datetime.today().strftime('%M') >= section1_starttime[-2:]:
        get_beacon(section1_curriculum)
    elif datetime.datetime.today().strftime('%H:%M') == section2_starttime or datetime.datetime.today().strftime('%H') < section2_endtime[:2] and datetime.datetime.today().strftime('%M') >= section2_starttime[-2:]:
        get_beacon(section2_curriculum)
    elif datetime.datetime.today().strftime('%H:%M') == section3_starttime or datetime.datetime.today().strftime('%H') < sectoin3_endtime[:2] and datetime.datetime.today().strftime('%M') >= section3_starttime[-2:]:
        get_beacon(section3_curriculum)
    elif datetime.datetime.today().strftime('%H:%M') == section4_starttime or datetime.datetime.today().strftime('%H') < sectoin4_endtime[:2] and datetime.datetime.today().strftime('%M') >= section4_starttime[-2:]:
        get_beacon(section4_curriculum)
    elif datetime.datetime.today().strftime('%H:%M') == section5_starttime or datetime.datetime.today().strftime('%H') < sectoin5_endtime[:2] and datetime.datetime.today().strftime('%M') >= section5_starttime[-2:]:
        get_beacon(section5_curriculum)
    elif datetime.datetime.today().strftime('%H:%M') == section6_starttime or datetime.datetime.today().strftime('%H') < sectoin6_endtime[:2] and datetime.datetime.today().strftime('%M') >= section6_starttime[-2:]:
        get_beacon(section6_curriculum)
    elif datetime.datetime.today().strftime('%H:%M') == section7_starttime or datetime.datetime.today().strftime('%H') < sectoin7_endtime[:2] and datetime.datetime.today().strftime('%M') >= section7_starttime[-2:]:
        get_beacon(section7_curriculum)
    elif datetime.datetime.today().strftime('%H:%M') == section8_starttime or datetime.datetime.today().strftime('%H') < section8_endtime[:2] and datetime.datetime.today().strftime('%M') >= section8_starttime[-2:]:
        get_beacon(section8_curriculum)
    elif datetime.datetime.today().strftime('%H:%M') == section9_starttime or datetime.datetime.today().strftime('%H') < section9_endtime[:2] and datetime.datetime.today().strftime('%M') >= section9_starttime[-2:]:
        get_beacon(section9_curriculum)
    elif datetime.datetime.today().strftime('%H:%M') == section10_starttime or datetime.datetime.today().strftime('%H') < sectoin10_endtime[:2] and datetime.datetime.today().strftime('%M') >= section10_starttime[-2:]:
        get_beacon(section10_curriculum)
    elif datetime.datetime.today().strftime('%H:%M') == section11_starttime or datetime.datetime.today().strftime('%H') < section11_endtime[:2] and datetime.datetime.today().strftime('%M') >= section11_starttime[-2:]:
        get_beacon(section11_curriculum)
    elif datetime.datetime.today().strftime('%H:%M') == section12_starttime or datetime.datetime.today().strftime('%H') < sectoin12_endtime[:2] and datetime.datetime.today().strftime('%M') >= section12_starttime[-2:]:
        get_beacon(section12_curriculum)
    elif datetime.datetime.today().strftime('%H:%M') == section13_starttime or datetime.datetime.today().strftime('%H') < section13_endtime[:2] and datetime.datetime.today().strftime('%M') >= section13_starttime[-2:]:
        get_beacon(section13_curriculum)
    elif datetime.datetime.today().strftime('%H:%M') == section14_starttime or datetime.datetime.today().strftime('%H') < sectoin14_endtime[:2] and datetime.datetime.today().strftime('%M') >= section14_starttime[-2:]:
        get_beacon(section14_curriculum)
    else:
        starter()

    total_timer()



# starter()
total_timer()
